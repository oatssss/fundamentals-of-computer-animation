package comp559.fluid;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point2f;
import javax.vecmath.Tuple2f;
import javax.vecmath.Vector2f;

import org.netlib.util.doubleW;
import org.netlib.util.floatW;
import org.netlib.util.intW;

import mintools.parameters.DoubleParameter;
import mintools.parameters.IntParameter;
import mintools.swing.VerticalFlowPanel;

/**
 * Eulerian fluid simulation class. 
 * 
 * This follows closely the solver documented in J. Stam GDC 2003, specifically
 * this uses a non staggered grid, with an extra set of grid cells to help enforce
 * boundary conditions
 * 
 * @author kry
 */
public class Fluid {
    
    final static int DIM = 2;
    
    /** velocity, non-staggered, packed (first index is the dimension), public for achievement checking */ 
    public float[][] U0;
    
    /** temporary velocity variable */
    private float[][] U1;          
       
    /** temperature (packed)*/
    public float[] temperature0;
    
    /** temperature (packed) temporary variable*/
    private float[] temperature1;
        
    private IntParameter Nval = new IntParameter( "grid size (requires reset)", 16, 8, 256 );
    
    /** Number of grid cells (not counting extra boundary cells */
    public int N = 16;
    
    /** Dimension of each grid cell */
    public float dx = 1;
    
    /** time elapsed in the fluid simulation */
    public double elapsed;

    /**
     * Sources of heat and cold
     */
    public List<Source> sources = new LinkedList<Source>();
    
    /**
     * initialize memory
     */
    public void setup() {
        elapsed = 0;
        N = Nval.getValue();        
        dx = 1.0f / N; // we choose the domain size here to be 1 unit square!
        
        int np2s = (N+2)*(N+2);
        U0 = new float[2][np2s];
        U1 = new float[2][np2s];
        temperature0 = new float[np2s];
        temperature1 = new float[np2s];
    }

    /**
     * Compute the index 
     * @param i 
     * @param j 
     * @return the index in the flattened array
     */
    public int IX( int i, int j ) {
        return i*(N+2) + j;
    }
    
    /** 
     * Gets the velocity at the given point using interpolation 
     * @param x
     * @param vel
     */
    public void getVelocity( Tuple2f x, Tuple2f vel ) {
        getVelocity( x, U0, vel );
    }
    
    /** 
     * Gets the velocity in the provided velocity field at the given point using interpolation
     * @param x
     * @param U
     * @param vel
     */
    private void getVelocity( Tuple2f x, float[][] U, Tuple2f vel ) {
        vel.x = interpolate( x, U[0] );
        vel.y = interpolate( x, U[1] );
    }
    
    /**
     * Interpolates the given scalar field
     * @param x
     * @param s
     * @return interpolated value
     */
    public float interpolate( Tuple2f x, float[] s ) {
        int i,j;
        float wx, wy;
        i = (int) Math.floor( x.x / dx - 0.5 );
        j = (int) Math.floor( x.y / dx - 0.5 );
        wx = x.x / dx - 0.5f - i;
        wy = x.y / dx - 0.5f - j;
        float val = ( (i>=0 && j>=0 && i<=N+1 && j<=N+1 )          ? s[IX(i,j)]*(1-wx)*(1-wy) : 0 ) + 
                     ( (i+1>=0 && j>=0 && i+1<=N+1 && j<=N+1 )     ? s[IX(i+1,j)]*wx*(1-wy) : 0 ) +
                     ( (i>=0 && j+1>=0 && i<=N+1 && j+1<=N+1 )     ? s[IX(i,j+1)]*(1-wx)*wy : 0 ) +
                     ( (i+1>=0 && j+1>=0 && i+1<=N+1 && j+1<=N+1 ) ? s[IX(i+1,j+1)]*wx*wy : 0 );
        return val;
    }
    
    /** 
     * Performs a simple Forward Euler particle trace using the current velocity field 
     * @param x0
     * @param h
     * @param x1
     */
    public void traceParticle( Point2f x0, float h, Point2f x1 ) {
        traceParticle( x0, U0, h, x1 );        
    }
    
    /** 
     * Performs a simple particle trace using Forward Euler
     * (Note that this could be something higher order or adative)
     * x1 = x0 + h * U(x0)
     * @param x0
     * @param U
     * @param h
     * @param x1
     */
    private void traceParticle( Point2f x0, float[][] U, float h, Point2f x1 ) {
        Vector2f vel = new Vector2f();
        x1.set( x0 );
        getVelocity(x1, U, vel);
        vel.scale(h);            
        x1.add( vel );
    }
    
    private Point2f mouseX1 = new Point2f();
    
    private Point2f mouseX0 = new Point2f();
    
    private void addMouseForce( float[][] U, float dt ) {
                
        Vector2f f = new Vector2f();
        f.sub( mouseX1, mouseX0 );
        float d = mouseX0.distance(mouseX1);
        if ( d < 1e-6 ) return;
        f.scale( mouseForce.getFloatValue() );
        // go along the path of the mouse!
        Point2f x = new Point2f();
        int num = (int) (d/dx + 1);
        for ( int i = 0; i <= num; i++ ) {
            x.interpolate(mouseX0,mouseX1, (float)i / num );
            addForce( U, dt, x, f );
        }
        mouseX0.set( mouseX1 );
    }
    
    /**
     * Sets the mouse location in the fluid for doing dragging interaction
     * @param x0 previous location
     * @param x1 current location
     */
    public void setMouseMotionPos( Point2f x0, Point2f x1 ) {
        mouseX0.set( x0 );
        mouseX1.set( x1 );
    }
    
    /**
     * Adds a force at a given point in the provided velocity field.
     * @param U    velocity field
     * @param dt   time step
     * @param x    location
     * @param f    force
     */
    private void addForce( float[][] U, float dt, Tuple2f x, Tuple2f f ) {
        addSource( U[0], dt, x, f.x );
        addSource( U[1], dt, x, f.y );
    }
     
    /**
     * Adds the time step scaled amount to the provided scalar field at the specified location x in an interpolated manner.
     * @param S    scalar field
     * @param dt   time step
     * @param x    location
     * @param a    amount
     */
    private void addSource( float[] S, float dt, Tuple2f x, float a ) {
        int i = (int) Math.floor( x.x / dx - 0.5 );
        int j = (int) Math.floor( x.y / dx - 0.5 );
        float wx = x.x / dx - 0.5f - i;
        float wy = x.y / dx - 0.5f - j;
        if ( i>=0 && j>=0 && i<=N+1 && j<=N+1 )          S[IX(i,j)]     += (1-wx)*(1-wy)* dt * a;
        if ( i+1>=0 && j>=0 && i+1<=N+1 && j<=N+1 )      S[IX(i+1,j)]   += wx*(1-wy) * dt * a;
        if ( i>=0 && j+1>=0 && i<=N+1 && j+1<=N+1 )      S[IX(i,j+1)]   += (1-wx)*wy * dt * a;
        if ( i+1>=0 && j+1>=0 && i+1<=N+1 && j+1<=N+1 )  S[IX(i+1,j+1)] += wx*wy * dt * a;
    }
    
    /**
     * Gets the average temperature of the continuum.
     * (use this in computing bouyancy forces)
     * @return
     */
    public double getReferenceTemperature() {
    	int count = 0;
        double referenceTemperature = 0;
        for ( int i = 1; i <= N; i++ ) {
            for ( int j = 1; j <= N; j++ ) {
                referenceTemperature += temperature0[IX(i,j)];
                count++;
            }
        }
        referenceTemperature /= count;
        return referenceTemperature;
    }
    
    private void diffuse( int b, float[] S, float[] S0, float diff, float dt ) {
    	
    	float a = dt*diff*N*N;
    	
    	// Use Gauss-Seidel relaxation
    	for (int k = 0; k < iterations.getValue(); k++) {
			for (int i = 1; i <= N; i++) {
				for (int j = 1; j <= N; j++) {
					S[IX(i,j)] = (S0[IX(i,j)] + a*(S[IX(i-1,j)] + S[IX(i+1,j)] + S[IX(i,j-1)] + S[IX(i,j+1)]))/(1+4*a);
				 }
			}
			
			SetBounds(b, S);
		}
    }
    
    private void transport( int b, float[] S, float [] S0, float dt ) {
    	
    	for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N; j++) {
				// Get the center of this cell as a coordinate
				float x = (i+0.5f)*dx;
				float y = (j+0.5f)*dx;
				Point2f x0 = new Point2f(x, y);
				Point2f x1 = new Point2f();
				
				// Use a negative time step to back trace
				traceParticle(x0, -stepSize.getFloatValue(), x1);
				S[IX(i, j)] = interpolate(x1, S0);
			}
		}
    	
    	SetBounds(b, S);
    }
    
    private void project( float[] V_x, float[] V_y, float[] p, float[] div ) {
    	
    	for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N; j++) {
				div[IX(i, j)] = -0.5f*dx*(V_x[IX(i+1,j)]-V_x[IX(i-1,j)]+V_y[IX(i,j+1)]-V_y[IX(i,j-1)]);
				p[IX(i, j)] = 0;
			}
		}
    	SetBounds(0, div);
    	SetBounds(0, p);
    	
    	// Use Gauss-Seidel relaxation
    	for (int k = 0; k < iterations.getValue(); k++) {
			for (int i = 1; i <= N; i++) {
				for (int j = 1; j <= N; j++) {
					p[IX(i,j)] = (div[IX(i,j)]+p[IX(i-1,j)]+p[IX(i+1,j)]+p[IX(i,j-1)]+p[IX(i,j+1)])/4;
				}
			}
			SetBounds(0, p);
		}
    	
    	for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N; j++) {
				V_x[IX(i,j)] -= 0.5*(p[IX(i+1,j)]-p[IX(i-1,j)])/dx;
				V_y[IX(i,j)] -= 0.5*(p[IX(i,j+1)]-p[IX(i,j-1)])/dx;
			}
    	}
    	SetBounds(1, V_x);
    	SetBounds(2, V_y);
    }
    
    private void SetBounds( int b, float[] S ) {
    	for (int i = 1; i <= N; i++) {
    		S[IX(0  ,i)] = b==1 ? -S[IX(1,i)] : S[IX(1,i)];
    		S[IX(N+1,i)] = b==1 ? -S[IX(N,i)] : S[IX(N,i)];
    		S[IX(i,0  )] = b==2 ? -S[IX(i,1)] : S[IX(i,1)];
    		S[IX(i,N+1)] = b==2 ? -S[IX(i,N)] : S[IX(i,N)];
		}
    	S[IX(0  ,  0)] = 0.5f*(S[IX(1,  0)]+S[IX(0  ,1)]);
    	S[IX(0  ,N+1)] = 0.5f*(S[IX(1,N+1)]+S[IX(0  ,N)]);
    	S[IX(N+1,0  )] = 0.5f*(S[IX(N,0  )]+S[IX(N+1,1)]);
    	S[IX(N+1,N+1)] = 0.5f*(S[IX(N,N+1)]+S[IX(N+1,N)]);
    }
    
    private void dens_step(float[] S, float[] S0, float diff, float dt) {
    	
    	transport(0, S0, S, dt);		// S has the previous temperatures
    	diffuse(0, S, S0, diff, dt);	// Swap S0 and S, S will have the final temperatures
    }
    
    private void vel_step(float[] V_x, float[] V_y, float[] V0_x, float[] V0_y, float visc, float dt) {
    	
    	diffuse(1, V0_x, V_x, visc, dt);	// V has previous velocities
    	diffuse(2, V0_y, V_y, visc, dt);
    	project(V0_x, V0_y, V_x, V_y);
    	transport(1, V_x, V0_x, dt);		// Swap V and V0, V will have the final velocities
    	transport(2, V_y, V0_y, dt);
    	project(V_x, V_y, V0_x, V0_y);
    }
    
    /**
     * Advances the state of the fluid by one time step
     */
    public void step() {
    	
        float dt = stepSize.getFloatValue();
        
        addMouseForce( U0, dt );
                
        // FINISHED : Use sources to change temperatures in the temperature0 scalar field
        for (Source source : this.sources) {
        	addSource(temperature0, dt, source.location, source.amount);
		}
        
        // FINISHED : Use temperature scalar field to apply buoyancy forces to the velocity field
        for (int i = 1; i <= N; i++) {
			for (int j = 1; j <= N; j++) {
				U0[1][IX(i, j)] += buoyancy.getFloatValue()*dt*((float)getReferenceTemperature() - temperature0[IX(i, j)]);
			}
		}

        // FINISHED : do the velocity step
        vel_step(U0[0], U0[1], U1[0], U1[1], viscosity.getFloatValue(), dt);

        // FINISHED : do the scalar step;        
        dens_step(temperature0, temperature1, diffusion.getFloatValue(), dt);
    	
        elapsed += dt;
    }
        
    private DoubleParameter viscosity = new DoubleParameter( "viscosity", 1e-6, 1e-8, 1 );
    private DoubleParameter diffusion = new DoubleParameter( "diffusion", 1e-6, 1e-8, 1 );
    private DoubleParameter buoyancy = new DoubleParameter( "buoyancy", 0.1, -1, 1 );
    private IntParameter iterations = new IntParameter( "Gauss Seidel iterations", 30, 20, 200 );    
    private DoubleParameter mouseForce = new DoubleParameter( "mouse force", 1e2, 1, 1e3 );
    
    /** step size of the simulation */
    public DoubleParameter stepSize = new DoubleParameter( "step size", 0.1, 0.001, 1 );
    
    /**
     * Get the parameters for the fluid 
     * @return a control panel
     */
    public JPanel getControls() {
        VerticalFlowPanel vfp = new VerticalFlowPanel();
        VerticalFlowPanel vfp1 = new VerticalFlowPanel();
        vfp1.setBorder(new TitledBorder("Fluid properties"));
        vfp1.add( viscosity.getSliderControls(true) );
        vfp1.add( diffusion.getSliderControls(true) );
        vfp1.add( buoyancy.getSliderControls(false) );
        vfp1.add( mouseForce.getSliderControls(true) );
        VerticalFlowPanel vfp2 = new VerticalFlowPanel();
        vfp2.setBorder(new TitledBorder("Fluid solver properties"));        
        vfp2.add( stepSize.getSliderControls(true ) ); 
        vfp2.add( iterations.getSliderControls() );
        vfp2.add( Nval.getSliderControls() );
        vfp.add( vfp1.getPanel() );
        vfp.add( vfp2.getPanel() );
        return vfp.getPanel();
    }
}
