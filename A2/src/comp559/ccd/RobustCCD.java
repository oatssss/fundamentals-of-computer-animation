package comp559.ccd;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

import org.netlib.util.booleanW;
import org.netlib.util.doubleW;

import comp559.ccdAchievements.j;

/**
 * Implementation of a robust collision detection.
 * @author kry
 */
public class RobustCCD {

    double restitution;
    
    double H;
    
    /**
     * Creates the new continuous collision detection and response object
     */
    public RobustCCD() {
        // do nothing
    }
    
    /**
     * Try to deal with contacts before they happen
     * @param h
     * @param system
     */
    public void applyRepulsion( double h, ParticleSystem system ) {
    	
    	for (Spring spring : system.springs) {
			for (Particle particle : system.particles) {
				handleRepulsions(spring, particle, h);
			}
		}
    }
    
    private void handleRepulsions(Spring spring, Particle particle, double h) {
    	
    	Particle A = spring.p1;
    	Particle B = spring.p2;
    	Particle C = particle;
    	
    	if (C == A || C == B)
    		{ return; }
    	
    	// Obtain alpha to find out where along AB the particle C lies
    	Vector2d AB = new Vector2d(B.p.x - A.p.x, B.p.y - A.p.y);
    	Vector2d AC = new Vector2d(C.p.x - A.p.x, C.p.y - A.p.y);
    	Vector2d AB_norm = new Vector2d(AB);
    	AB_norm.normalize();
    	double scalarProjection = AC.dot(AB_norm);
    	double alpha = scalarProjection / AB.length();
    	
    	// Different cases for along the edge and at the endpoints
    	double overlap;
    	Vector2d normal;
    	Vector2d relativeVelocity;
    	// Do particle-edge repulsion
    	if (alpha >= 0 - SanityCheck.eps && alpha <= 1 + SanityCheck.eps) {
    		// Find the rejection of AC from AB to get its magnitude to compare to the buffer distance
        	Vector2d AC_projAB = new Vector2d(AB_norm);
        	AC_projAB.scale(scalarProjection);
        	Vector2d AC_rejAB = new Vector2d(AC);
        	AC_rejAB.sub(AC_projAB);
        	double distance = AC_rejAB.length();
        	overlap = this.H - distance;
        	// Return if not in buffer zone
        	if (overlap < 0) {
				return;
			}
        	
    		// Find the contact normal using orthogonal vector
    		normal = new Vector2d(-AB.y, AB.x);
    		double towardsC = normal.dot(AC);
    		normal = (towardsC > 0) ? normal : (new Vector2d(AB.y, -AB.x)); // Negative dot product indicates obtuse angles between vectors
    		normal.normalize();
    		
    		// Find velocity at alpha point
    		Vector2d A_blend = new Vector2d(A.v);
    		A_blend.scale(1-alpha);
    		Vector2d B_blend = new Vector2d(B.v);
    		B_blend.scale(alpha);
    		Vector2d alphaDot = new Vector2d(0, 0);
    		alphaDot.add(A_blend);
    		alphaDot.add(B_blend);
    		
    		// Calculate relative velocity along normal
    		Vector2d normal_transpose = new Vector2d(normal.y, normal.x);
    		Vector2d cSubAlpha = new Vector2d(C.v);
    		cSubAlpha.sub(alphaDot);
    		relativeVelocity = new Vector2d(normal_transpose.x*cSubAlpha.x, normal_transpose.y*cSubAlpha.y);
		}
    	// Do particle-particle repulsion
    	else {
			// Determine whether C is nearing A or B
    		if (alpha < 0) {	// Closer to A
				double distance = AC.length();
				overlap = this.H - distance;
				
				// Return if not in buffer zone
	        	if (overlap < 0) {
					return;
				}
				normal = new Vector2d(AC);
				normal.normalize();
				
				// Calculate relative velocity along normal
	    		Vector2d normal_transpose = new Vector2d(normal.y, normal.x);
	    		Vector2d cSubA = new Vector2d(C.v);
	    		cSubA.sub(A.v);
	    		relativeVelocity = new Vector2d(normal_transpose.x*cSubA.x, normal_transpose.y*cSubA.y);
			}
    		else {	// Closer to B
    			Vector2d BC = new Vector2d(C.p.x - B.p.x, C.p.y - B.p.y);
    			double distance = BC.length();
    			overlap = this.H - distance;
				
				// Return if not in buffer zone
	        	if (overlap < 0) {
					return;
				}
				normal = new Vector2d(BC);
				normal.normalize();
				
				// Calculate relative velocity along normal
	    		Vector2d normal_transpose = new Vector2d(normal.y, normal.x);
	    		Vector2d cSubB = new Vector2d(C.v);
	    		cSubB.sub(B.v);
	    		relativeVelocity = new Vector2d(normal_transpose.x*cSubB.x, normal_transpose.y*cSubB.y);
			}
		}
    	
    	// We now have the normal, overlap, and relative velocity and can now compute the impulse
    	double velocity_normalComponent = relativeVelocity.dot(normal);
    	
    	if (velocity_normalComponent > 0.1*overlap/h)
    		{ return; }
    	
    	double a_mass = spring.p1.pinned ? Double.POSITIVE_INFINITY : spring.p1.mass;
		double b_mass = spring.p2.pinned ? Double.POSITIVE_INFINITY : spring.p2.mass;
		double c_mass = particle.pinned ? Double.POSITIVE_INFINITY : particle.mass;
    	
    	// Use impulse formula given by Bridson paper
    	double j_c_scalar = -Math.min(Spring.k*overlap*h, (c_mass)*((0.1*overlap/h) - velocity_normalComponent));
    	Vector2d j_c = new Vector2d(normal);
    	j_c.scale(j_c_scalar);
    	Vector2d j_a = new Vector2d(j_c);
		j_a.scale(-(1-alpha));
		Vector2d j_b = new Vector2d(j_c);
		j_b.scale(-alpha);
		Vector2d normal_transpose = new Vector2d(normal.y, normal.x);
		Vector2d j_a_n = new Vector2d(normal_transpose.x*j_a.x, normal_transpose.y*j_a.y);
		j_a_n.scale(1/a_mass);
		Vector2d j_b_n = new Vector2d(normal_transpose.x*j_b.x, normal_transpose.y*j_b.y);
		j_b_n.scale(1/b_mass);
		Vector2d j_c_n = new Vector2d(normal_transpose.x*j_c.x, normal_transpose.y*j_c.y);
		j_c_n.scale(1/c_mass);
		
		// Update particle velocities
		A.v.add(j_a_n);
		B.v.add(j_b_n);
		C.v.add(j_c_n);
		
//		A.v = j_a_n;
//		B.v = j_b_n;
//		C.v = j_c_n;
    }
    
    /**
     * Checks all collisions in interval t to t+h
     * @param h
     * @param system 
     * @return true if all collisions resolved
     */
    public boolean check( double h, ParticleSystem system ) {        

    	boolean noCollisions = false;
    	int maxIterations = 50;
    	while (!noCollisions) {
    		noCollisions = true;
			for (Spring spring : system.springs) {
				for (Particle particle : system.particles) {
					if (handleCollision(spring, particle, h))
						{ noCollisions = false; }
				}
			}
			maxIterations--;
			
			if (maxIterations <= 0)
				{ return false; }
		}
        
        return true;
    }
    
    
    private boolean handleCollision(Spring spring, Particle particle, double h) {
    	
    	Particle A = spring.p1;
    	Particle B = spring.p2;
    	Particle C = particle;
    	
    	if (C == A || C == B)
			{ return false; }
    	
    	// Calculate the 'h' terms to solve the quadratic
    	double c3 = A.p.x*(B.p.y-C.p.y) + B.p.x*(C.p.y-A.p.y) + C.p.x*(A.p.y-B.p.y); // constant term
    	double c2 = A.p.x*(B.v.y-C.v.y) + A.v.x*(B.p.y-C.p.y) + B.p.x*(C.v.y-A.v.y) + B.v.x*(C.p.y-A.p.y) + C.p.x*(A.v.y-B.v.y) + C.v.x*(A.p.y-B.p.y); // h term
    	double c1 = A.v.x*(B.v.y-C.v.y) + B.v.x*(C.v.y-A.v.y) + C.v.x*(A.v.y-B.v.y); // h^2 term
    	double t;
    	
    	// If h^2 is zero, just solve a linear case (c2*h + c3 = 0) => (h = -c3/c2)
    	if (c1 == 0) {
    		if (c2 == 0) {
				t = Double.POSITIVE_INFINITY;
			}
    		else {
    			t = -c3/c2;
    		}
		}
    	else if (c2 == 0) {
    		if ((-c3 >= 0 && c1 > 0) || (c3 >= 0 && -c1 > 0)) {
				t = Math.sqrt(-c3/c1);
			}
    		else {
				t = Double.POSITIVE_INFINITY;
			}
		}
    	else {
    		// Do the plus case in the quadratic
        	double tPlus = (-c2 + Math.sqrt(Math.pow(c2, 2) - 4*c1*c3))/(2*c1);
        	tPlus = Double.isNaN(tPlus) ? -1 : tPlus;
        	tPlus = (tPlus < 0) ? Double.POSITIVE_INFINITY : tPlus; 
        	// Do the minus case
        	double tMinus = (-c2 - Math.sqrt(Math.pow(c2, 2) - 4*c1*c3))/(2*c1);
        	tMinus = Double.isNaN(tMinus) ? -1 : tMinus;
        	tMinus = (tMinus < 0) ? Double.POSITIVE_INFINITY : tMinus;
        	// Only look at the soonest colinear
        	t = Math.min(tPlus, tMinus);
		}
    	
    	// t must exist in (0,h] for a collision
    	if (t > (h + SanityCheck.eps) || t < (0 + SanityCheck.eps))
    		{ return false; }
    	
    	// Find the positions of the particles at t
    	Vector2d AdotT = new Vector2d(A.v);
    	AdotT.scale(t);
    	Vector2d BdotT = new Vector2d(B.v);
    	BdotT.scale(t);
    	Vector2d CdotT = new Vector2d(C.v);
    	CdotT.scale(t);
    	
    	Point2d A_t = new Point2d(A.p);
    	A_t.add(AdotT);
    	Point2d B_t = new Point2d(B.p);
    	B_t.add(BdotT);
    	Point2d C_t = new Point2d(C.p);
    	C_t.add(CdotT);
    	
    	// Obtain vectors AB and AC so we can project AC onto AB and get alpha
    	Vector2d AB = new Vector2d(B_t.x - A_t.x, B_t.y - A_t.y);
    	Vector2d AC = new Vector2d(C_t.x - A_t.x, C_t.y - A_t.y);
    	Vector2d AB_norm = new Vector2d(AB);
    	AB_norm.normalize();
    	double alpha = AC.dot(AB_norm) / AB.length();	// alpha is really the scalar projection divided by the magnitude of AB
    	
    	// alpha must be in range [0-epsilon,1+epsilon] otherwise particle does not collide (colinear, but not between A and B)
    	if (alpha < 0 - SanityCheck.eps || alpha > 1 + SanityCheck.eps)
    		{ return false; }
    	
    	// Clamp alpha to [0,1]
    	alpha = (alpha < 0) ? 0 : alpha;
    	alpha = (alpha > 1) ? 1 : alpha;
    	
    	// Find the contact normal using orthogonal vector
		Vector2d normal = new Vector2d(-AB.y, AB.x);
		double direction = normal.dot(particle.v);
		if (direction < 0)
			{ normal.negate(); }
//		normal = (direction < 0) ? normal : (new Vector2d(AB.y, -AB.x)); // Negative dot product indicates obtuse angles between vectors
		normal.normalize();
		
		// Get '-' velocity for the point on the spring at alpha
		Vector2d A_blend = new Vector2d(A.v);
		A_blend.scale(1-alpha);
		Vector2d B_blend = new Vector2d(B.v);
		B_blend.scale(alpha);
		Vector2d alphaDot_minus = new Vector2d(0, 0);
		alphaDot_minus.add(A_blend);
		alphaDot_minus.add(B_blend);
		
		// Calculate relative velocity for the 'before' stage
		Vector2d normal_transpose = new Vector2d(normal.y, normal.x);
		Vector2d cSubAlpha = new Vector2d(C.v);
		cSubAlpha.sub(alphaDot_minus);
		Vector2d velocity_relNormal = new Vector2d(normal_transpose.x*cSubAlpha.x, normal_transpose.y*cSubAlpha.y);
		
		// Calculate 'j' the impulse for all three points
		double a_mass = spring.p1.pinned ? Double.POSITIVE_INFINITY : spring.p1.mass;
		double b_mass = spring.p2.pinned ? Double.POSITIVE_INFINITY : spring.p2.mass;
		double c_mass = particle.pinned ? Double.POSITIVE_INFINITY : particle.mass;
		double j_scalar = -(1+restitution)/((1/c_mass)+(Math.pow(1-alpha, 2)/a_mass)+(Math.pow(alpha, 2)/b_mass));
		Vector2d j_c = new Vector2d(velocity_relNormal);
		j_c.scale(j_scalar);
		Vector2d j_a = new Vector2d(j_c);
		j_a.scale(-(1-alpha));
		Vector2d j_b = new Vector2d(j_c);
		j_b.scale(-alpha);
		
		// Update the velocities
		
//		Vector2d j_a_n = new Vector2d(normal.x*j_a.x, normal.y*j_a.y);
//		j_a_n.scale(1/a_mass);
//		Vector2d j_b_n = new Vector2d(normal.x*j_b.x, normal.y*j_b.y);
//		j_b_n.scale(1/b_mass);
//		Vector2d j_c_n = new Vector2d(normal.x*j_c.x, normal.y*j_c.y);
//		j_c_n.scale(1/c_mass);
//		
//		A.v.add(j_a_n);
//		B.v.add(j_b_n);
//		C.v.add(j_c_n);
		
		A.v = j_a;
		B.v = j_b;
		C.v = j_c;
    	
    	return true; // Return true to indicate a collision was found
	}  
}
