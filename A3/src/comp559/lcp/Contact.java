package comp559.lcp;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

/**
 * Implementation of a contact constraint.
 * @author kry
 */
public class Contact {

    /** Next available contact index, used for determining which rows of the jacobian a contact uses */
    static public int nextContactIndex = 0;
    
    /** Index of this contact, determines its rows in the jacobian */
    int index;
    
    /** First RigidBody in contact */
    RigidBody body1;
    
    /** Second RigidBody in contact */
    RigidBody body2;
    
    /** Contact normal in world coordinates */
    Vector2d normal = new Vector2d();
    
    /** Position of contact point in world coordinates */
    Point2d contactW = new Point2d();
    
    /** A matrix of 2 dimensional vectors to hold the contact jacobian */
    Vector2d contactJacobian[][];
    
    /**
     * Creates a new contact, and assigns it an index
     * @param body1
     * @param body2
     * @param contactW
     * @param normal
     */
    public Contact( RigidBody body1, RigidBody body2, Point2d contactW, Vector2d normal ) {
        this.body1 = body1;
        this.body2 = body2;
        this.contactW.set( contactW );
        this.normal.set( normal );        
        this.contactJacobian = new Vector2d[3][4]; 
        index = nextContactIndex++;
        
        // Calculate and store the contact Jacovian
        
        Vector2d radius1 = new Vector2d(this.contactW);
        radius1.sub(this.body1.x);
        Vector2d radius2 = new Vector2d(this.contactW);
        radius2.sub(this.body2.x);
        
        // Calculate the first row of the contact Jacobian (eq. 2)
        Vector2d neg_Normal = new Vector2d(this.normal);
        neg_Normal.negate();
        Vector2d neg_r1_cross_n = new Vector2d(-radius1.y*this.normal.x, radius1.x*this.normal.y);
        neg_r1_cross_n.negate();
        Vector2d r2_cross_n = new Vector2d(-radius2.y*this.normal.x, radius2.x*this.normal.y);
        this.contactJacobian[0][0] = neg_Normal;
        this.contactJacobian[0][1] = neg_r1_cross_n;
        this.contactJacobian[0][2] = this.normal;
        this.contactJacobian[0][3] = r2_cross_n;
        
        // Calculate the second row (eq. 6)
        Vector2d tangent1 = new Vector2d(-radius1.y, radius1.x);
        tangent1.normalize();
    	Vector2d neg_tangent1 = new Vector2d(tangent1);
    	neg_tangent1.negate();
    	Vector2d neg_r1_cross_t1 = new Vector2d(-radius1.y*tangent1.x, radius1.x*tangent1.y);
    	neg_r1_cross_t1.negate();
    	Vector2d r2_cross_t1 = new Vector2d(-radius2.y*tangent1.x, radius2.x*tangent1.y);
    	this.contactJacobian[1][0] = neg_tangent1;
    	this.contactJacobian[1][1] = neg_r1_cross_t1;
    	this.contactJacobian[1][2] = tangent1;
    	this.contactJacobian[1][3] = r2_cross_t1;
    	
    	// Calculate the third row
    	Vector2d tangent2 = new Vector2d(-radius2.y, radius2.x);
        tangent2.normalize();
    	Vector2d neg_tangent2 = new Vector2d(tangent2);
    	neg_tangent2.negate();
    	Vector2d neg_r1_cross_t2 = new Vector2d(-radius1.y*tangent2.x, radius1.x*tangent2.y);
    	neg_r1_cross_t1.negate();
    	Vector2d r2_cross_t2 = new Vector2d(-radius2.y*tangent2.x, radius2.x*tangent2.y);
    	this.contactJacobian[2][0] = neg_tangent2;
    	this.contactJacobian[2][1] = neg_r1_cross_t2;
    	this.contactJacobian[2][2] = tangent2;
    	this.contactJacobian[2][3] = r2_cross_t2;
    }
    
    /**
     * Draws the contact points
     * @param drawable
     */
    public void display( GLAutoDrawable drawable ) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glPointSize(3);
        gl.glColor3f(.7f,0,0);
        gl.glBegin( GL.GL_POINTS );
        gl.glVertex2d(contactW.x, contactW.y);
        gl.glEnd();
    }
    
    /**
     * Draws the connections between bodies to visualize the 
     * the adjacency structure of the matrix as a graph.
     * @param drawable
     */
    public void displayConnection( GLAutoDrawable drawable ) {
        GL2 gl = drawable.getGL().getGL2();
        // draw a line between the two bodies but only if they're both not pinned
        if ( !body1.pinned && ! body2.pinned ) {
            gl.glLineWidth(2);
            gl.glColor4f(0,.3f,0, 0.5f);
            gl.glBegin( GL.GL_LINES );
            gl.glVertex2d(body1.x.x, body1.x.y);
            gl.glVertex2d(body2.x.x, body2.x.y);
            gl.glEnd();
        }
    }
    
}
