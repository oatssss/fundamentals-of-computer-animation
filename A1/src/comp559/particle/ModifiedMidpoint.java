package comp559.particle;

public class ModifiedMidpoint implements Integrator {

    @Override
    public String getName() {
        return "modified midpoint";
    }

    @Override
    public void step(double[] y, int n, double t, double h, double[] yout, Function derivs) {

    	// Create a new array to hold the first euler step (k1)
    	double[] k1 = new double[n];
    	
    	// Get dydt using the derivs function
    	derivs.derivs(t, y, k1);
    	
    	// Take a half-step in the direction of k1
        for (int i = 0; i < n; i++) {
			k1[i] = y[i] + (2.0/3.0)*h*k1[i]; // 2/3 for modified midpoint
		}
        
        // Calculate a second derivative using the midpoint (k2)
        derivs.derivs(t, k1, yout);
        
        // Take a full-step in the direction of the second derivative (k2), but from the original point
        for (int i = 0; i < n; i++) {
			yout[i] = y[i] + h*yout[i];
		}
    }

}
