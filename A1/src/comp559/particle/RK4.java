package comp559.particle;

public class RK4 implements Integrator {
    
    @Override
    public String getName() {
        return "RK4";
    }

    @Override
    public void step(double[] y, int n, double t, double h, double[] yout, Function derivs) {
        
    	/* 
    	 * The link below gave me a better understanding of the runge-kutta methods
    	 * 
    	 * http://graphics.cs.ucdavis.edu/~joy/ecs277/other-notes/Numerical-Methods-for-Particle-Tracing-in-Vector-Fields.pdf
    	 */
    	
    	// Create arrays to hold vectors k1, k2, k3, and k4
    	double[] k1 = new double[n];
    	double[] k2 = new double[n];
    	double[] k3 = new double[n];
    	double[] k4 = new double[n];
    	
    	// Get k1 using the derivs function
    	derivs.derivs(t, y, k1);
    	// Take a half-step in the direction of k1 from the original point
        for (int i = 0; i < n; i++) {
			yout[i] = y[i] + 0.5*h*k1[i];
		}
        
        // Get k2 from the midpoint (since we took a half-step) using the derivs function
        derivs.derivs(t, yout, k2);
        // Take a half-step in the direction of k2 from the original point
        for (int i = 0; i < n; i++) {
			yout[i] = y[i] + 0.5*h*k2[i];
		}
        
        // Get k3
        derivs.derivs(t, yout, k3);
        // Take a full-step in the direction of k3 from the original point
        for (int i = 0; i < n; i++) {
			yout[i] = y[i] + h*k3[i];
		}
        
        // Get k4
        derivs.derivs(t, yout, k4);
        
        // Piece everything together: Calculate [p0 + (1/6)(k1 + 2k2 + 2k3 + k4)]
        for (int i = 0; i < n; i++) {
			yout[i] = y[i] + (1.0/6.0)*h*(k1[i] + 2*k2[i] + 2*k3[i] + k4[i]);
		}
    }
}
