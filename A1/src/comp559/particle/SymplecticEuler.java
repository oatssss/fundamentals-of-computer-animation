package comp559.particle;

public class SymplecticEuler implements Integrator {

    @Override
    public String getName() {
        return "symplectic Euler";
    }

    @Override
    public void step(double[] y, int n, double t, double h, double[] yout, Function derivs) {
    	
    	// First get the derivatives so we have velocity and acceleration
    	derivs.derivs(t, y, yout);
    	
    	// Update y's velocity using the derived acceleration
    	for (int i = 2; i < n; i += 2) {
			yout[i] = y[i] + h*yout[i++];	// Done twice to avoid a nested for loop
			yout[i] = y[i] + h*yout[i++];
		}
    	
    	// Then update y's position using the updated y's velocity
    	for (int i = 0; i < n; i += 2) {
    		yout[i] = y[i] + h*yout[(i++)+2];
			yout[i] = y[i] + h*yout[(i++)+2];
		}
    }

}
