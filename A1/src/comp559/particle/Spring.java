package comp559.particle;

import javax.vecmath.Vector2d;

import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.Vector;

/**
 * Spring class for 599 assignment 1
 * @author kry
 */
public class Spring {

    Particle p1 = null;
    Particle p2 = null;
    
    /** Spring stiffness, sometimes written k_s in equations */
    public static double k = 1;
    /** Spring damping (along spring direction), sometimes written k_d in equations */
    public static double c = 1;
    /** Rest length of this spring */
    double l0 = 0;
    
    /**
     * Creates a spring between two particles
     * @param p1
     * @param p2
     */
    public Spring( Particle p1, Particle p2 ) {
        this.p1 = p1;
        this.p2 = p2;
        recomputeRestLength();
        p1.springs.add(this);
        p2.springs.add(this);
    }
    
    /**
     * Computes and sets the rest length based on the original position of the two particles 
     */
    public void recomputeRestLength() {
        l0 = p1.p0.distance( p2.p0 );
    }
    
    /**
     * Applies the spring force by adding a force to each particle
     */
    public void apply() {
        /* We will use p1 as particle A */
    	
    	// Vector going from b to a
    	Vector2d deltaX = new Vector2d();
    	deltaX.sub(this.p1.p, this.p2.p);
    	
    	// "Time"-derivative of deltaX (v_a - v_b)
    	Vector2d deltaV = new Vector2d();
    	deltaV.sub(this.p1.v, this.p2.v);
    	
    	// Magnitude of deltaX
    	double magX = deltaX.length();
    	
    	// Scale constant for f_a, taken from pixar's formula for a damped Hook's law spring
    	double kf_a = -(k*(magX - l0) + c*((deltaV.dot(deltaX))/magX))/magX;
    	
    	Vector2d f_a = new Vector2d(deltaX);
    	f_a.scale(kf_a); // Now actually scale it
    	
    	// f_b = -f_a
    	Vector2d f_b = new Vector2d(f_a);
    	f_b.negate();
    	
    	// Apply f_a and f_b to p1 and p2 respectively
    	this.p1.addForce(f_a);
    	this.p2.addForce(f_b);
    }
   
    /** TODO: the functions below are only for the backwards Euler solver */
    
    /**
     * Computes the force and adds it to the appropriate components of the force vector.
     * (This function is something you might use for a backward Euler integrator)
     * @param f
     */
    public void addForce(Vector f) {
        // TODO: (for bonus) FINISH THIS CODE (probably very simlar to what you did above)
        
    }
    
    /**
     * Adds this springs contribution to the stiffness matrix
     * @param dfdx
     */
    public void addDfdx( Matrix dfdx ) {
        // TODO: (for bonus) FINISH THIS CODE... necessary for backward euler integration
        
    }   
 
    /**
     * Adds this springs damping contribution to the implicit damping matrix
     * @param dfdv
     */
    public void addDfdv( Matrix dfdv ) {
        // TODO: FINISH THIS CODE... necessary for backward euler integration
        
    } 
    
}
